import VueRouter from 'vue-router'//导入vue路由插件
// 导入四个底部导航栏的组件
import HomeContainer from './components/tabber/HomeContainer.vue'
import MytbContainer from './components/tabber/MytbContainer.vue'
import ShopcarContainer from './components/tabber/ShopcarContainer.vue'
import SearchContainer from './components/tabber/SearchContainer.vue'
import GoodsList from './components/goods/GoodsList.vue'//导入首页--商品列表页
import NewsList from './components/news/NewsList.vue'//导入首页--新闻列表列
import Newsinfo from './components/news/Newsinfo.vue'//导入首页--新闻列表页--新闻详细页

//创建路由对象
var router =new VueRouter({
    routes: [
        // 配置四个底部导航栏的路由路径

        { path: '/', redirect:'/home' },
        { path: '/home', component: HomeContainer },
        { path: '/mytaobao', component: MytbContainer },
        { path: '/shopcar', component: ShopcarContainer },
        { path: '/search', component: SearchContainer },
        { path: '/home/goodsList', component: GoodsList },//设置首页--商品列表页
        { path: '/home/newsList', component: NewsList },//设置首页--新闻列表列
        { path: '/home/newsList/newsinfo/:newid', component: Newsinfo },//设置首页--新闻详细列
        
        
    ],
    linkActiveClass:'mui-active'//点击高亮
})

export default router//导出（暴露）该路由