//入口函数文件
console.log('准备就绪.....')

import Vue from 'vue'
import app from './App.vue'

import { Header, Swipe, SwipeItem} from 'mint-ui';//导入min-ui的Header组件 swipe组件
import './lib/mui/css/mui.min.css'//导入mui样式表
import './lib/mui/css/icons-extra.css'//导入mui样式表



Vue.component(Header.name, Header);//使用该组件

// 使用轮播图
Vue.component(Swipe.name, Swipe);
Vue.component(SwipeItem.name, SwipeItem);

// 1.1导入路由器
import VueRouter from 'vue-router'
// 1.2使用路由器
Vue.use(VueRouter)
// 1.3导入自己的router
import router from './router.js'
// 2.1导入数据加载的插件
import VueResource from 'vue-resource'
// 2.2使用该插件
Vue.use(VueResource)
//2.3设置请求的跟路径
Vue.http.options.root = 'http://www.liulongbin.top:3005/'
// 2.4设置全局的post请求的参数
Vue.http.options.emulateJSON = true;

var vm = new Vue({
    el: "#app",
    data: {
        
    },
    methods: {
        
    },
    render: c => c(app),
    // 1.4挂载到路由vue
    router

    /* render:function(createElements){
       createElements 是一个方法，调用这个方法，能够把指定的组件模板，渲染为HTML结构
         return createElements(app)
        注意：这里return的结果，会替换页面中el指定的那个容器
        } 
    */
    
})